#!/bin/bash


# DEBUG PURPOSE

# exit when any command fails
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT


# DEFINITIONS
challenges_dir=./challenges
compose_file=docker/docker-compose.yml
logfile=./dockers-starts.log

wake_up_containers() {
    for categorie in `ls $challenges_dir`;
    do
        echo "[*] Category : $categorie";
        for challenge in `ls $challenges_dir/$categorie`;
        do
            echo "--> [*] Challenge $challenge";
            chall_dir=$challenges_dir/$categorie/$challenge
            start_file=$chall_dir/start.sh
            if [ -f "$start_file" ]; then
                echo "--> [*] Starting challenge $challenge";
                pushd $chall_dir; ./start.sh; popd
                echo "$challenge:EXECUTED" >> $logfile
            else
                echo "--> [X] No start file for chall $challenge.";
                echo "$challenge:NULL" >> $logfile
            fi;
        done;
    done;
}

# MAIN

if [ -f $logfile ]; then
    mv $logfile $logfile".bak";
    touch $logfile;
fi;

wake_up_containers;

echo "All containers should be up (please check!)"
screen -ls

echo "Running autoheal"
pushd infra/scripts/; screen -dmS heal python autoheal.py; popd

