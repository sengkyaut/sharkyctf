#!/usr/bin/python3


import bitarray  # To manipulate binaries
import logging  # For debug messages
from PIL import Image  # To play with images
from mmap import mmap, PROT_READ
import re  # To do a regex when trying to extract a message


def strings(filename, n=6):
    with open(filename, 'rb') as f, mmap(f.fileno(), 0, prot=PROT_READ) as m:
        for match in re.finditer(('([\w/]{%s}[\w/]*)' % n).encode(), m):
            yield match.group(0)


class LSB():
    """
    An LSB class which will help to implement the steganography LSB method.
    This class will be tested on .bmp images with "RGB" mode.

    Note, works pretty well on .bmp and .png files, but not really on JPEG files
    due to compression.
    """
    def __init__(self, img, debug=True):
        self.img = img
        self.width, self.height = img.size
        self.pixels = img.load()  # Change this will automaticly change img
        if debug:
            self.debug = True
            self.logger = logging.getLogger()
        else:
            self.debug = False
    def print_pixels_colors(self):
        for width in range(0, self.width):
            for height in range(0, self.height):
                # pixel[width, height]
                print("pixel[{}, {}] : {}".format(
                    width,
                    height,
                    self.pixels[width, height]  # color
                ))
    def __str__(self):
        return "LSBObject: [width: {}, height: {}]".format(
            self.width,
            self.height
        )

    def get_bits_from_str(self, msg):
        """
        msg: bytes
        :returns: bitarray
        """
        ba = bitarray.bitarray()
        ba.frombytes(msg)
        return ba

    def change_lsb(self, color, bit):
        """
        Change the LSB of a color.
        color: The color (int)
        bit: The bit which represent the new LSB value (str)

        :returns: The color modified.
        """
        color = list(bin(color)[2:])
        color[-1] = str(int(bit))
        color = ''.join(color)
        color = int(color, 2)
        return color

    def insert(self, message, random=False):
        """
        Insert message through LSB inside the image.
        message: The message to insert (bytes)
        random: Whether insertion should be random or not (bool, default:False)

        :raises: ValueError When message is too long to be hidden.

        :returns: 0 if everything went well.
        """
        bits = self.get_bits_from_str(message)
        if self.debug:
            self.logger.info(
                "[*] Inserting message {} into image...".format(
                    message
                )
            )
        if not random:
            width = 0
            height = 0
            i = 0
            size_msg = len(bits)
            if self.debug:
                self.logger.debug(
                    "Number of bits of the message: {}".format(size_msg)
                )
                self.logger.debug(
                    "img size: ({}, {})".format(self.width, self.height)
                )
            while i < size_msg:
                r, g, b = self.pixels[width, height]
                r = self.change_lsb(r, bits[i])
                i += 1
                if i < size_msg:
                    g = self.change_lsb(g, bits[i])
                    i += 1
                if i < size_msg:
                    b = self.change_lsb(b, bits[i])
                    i += 1
                self.pixels[width, height] = (r, g, b)
                if i < size_msg:
                    # Ajust pixel pointers
                    width, height = self.next_pixel(width, height)
        return 0

    def extract_lsb_bits(self, seed=None, size=None):
        """
        Return a list of all lsb of each pixels bytes.

        Size: The potential number of bits in the message (int)
        """
        bits = []
        ended = False  # Just to win some times
        for height in range(0, self.height):
            for width in range(0, self.width):
                # pixel[width, height]
                r, g, b = self.pixels[width, height]
                # 0 when x % 2 == 0
                bits.append(str(r % 2))
                bits.append(str(g % 2))
                bits.append(str(b % 2))
                if size is not None and len(bits) > size:
                    ended = True
                    break
            if ended:
                break
        return bits

    def extract_messages(self, seed=None, size=None):
        """
        Try to convert every byte to a character
        Returns a list of all char made through lsb.

        Size: Potential number of bits in the message.
        """
        chars = []
        bits = self.extract_lsb_bits(seed=seed, size=size)
        i = 8
        nb_bits = len(bits)
        # Convert every byte to a char
        while i <= nb_bits - 8:
            char = ''.join(bits[i - 8:i])
            if self.debug:
                #self.logger.debug(
                #    "Extracting char through byte {}".format(char)
                #)
                pass
            i += 8
            char = int(char, 2)
            char = chr(char)
            chars.append(char)
        return ''.join(chars)

    def next_pixel(self, width, height):
        """
        Returns the next pixel indices values
        based upon current width and height values.
        :raise: ValueError when out of pixel
        """
        if(width + 1 >= self.width):
            if(height + 1 >= self.height):
                raise ValueError(
                    "Can not reach new pixel! Out of pixel. Message too long?"
                )
            else:
                height += 1
                width = 0
                if self.debug:
                    self.logger.debug(
                        "[*] End of column {} going to next line...".format(
                            height - 1
                        )
                    )
        else:
            width += 1
        return width, height


def main():
    logging.basicConfig(
        format='%(asctime)s %(levelname)s: %(message)s',
        level=logging.DEBUG,
        datefmt='%m/%d/%Y %I:%M:%S %p'
    )
    img = Image.open("../challenge/pretty_cat.png")
    img = img.convert("RGB")
    lsb = LSB(img)

    chars = lsb.extract_messages(size=2600)
    with open("output", "w") as f:
        f.write(chars)

    lsb.logger.info(
        "Output strings extracted:"
    )
    for w in strings("output"):
        print(w)
    lsb.logger.info(
        "See file output to get clearer message"
    )

if __name__ == "__main__":
    main()
