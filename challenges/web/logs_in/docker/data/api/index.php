<?php
    $host = 'mysql';
    $user = 'mainUser';
    $pass = 'th15Pa55w0rdIsTh3B35t';
    $dbname = 'db';
    $conn = new mysqli($host, $user, $pass, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if(isset($_GET['method'])){
      $log_method = $_GET['method'];
    } else {
      $log_method = 'GET';
    }

    $query = "SELECT id, log_method, log_date FROM logs WHERE log_method='$log_method'";


    if ($result = $conn->query($query)) {

        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $data[] = $row;
        }
        echo json_encode($data);

        $result->close();
    } else {
        ?>
        No Data :(
        <?php
    }
    $conn->close();
  ?>
