from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time
from threading import Thread

host = "logs_in-nginx"
url = "http://"+host+"/" # /rapport

def trig(drv):
    drv.get(url+"e48e13207341b6bffb7fb1622282247b/debug")

while(True):
    try:
        driver = webdriver.Remote(command_executor='http://logs_in-selenium:4444/wd/hub',
                                  desired_capabilities=DesiredCapabilities.FIREFOX)
        t = Thread(target=trig, args=(driver,))
        t.start()
        time.sleep(100)  # slow down
        driver.quit()
    except Exception as e:
        print(e)
