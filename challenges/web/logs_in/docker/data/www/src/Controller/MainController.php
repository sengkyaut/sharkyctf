<?php
namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use App\Service\CallAPI;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpKernel\Profiler\Profiler;

class MainController extends AbstractController
{

    /**
     * @Route("/")
     * @return Response
     */
    public function index(Profiler $profiler)
    {
        $profiler->enable();
        $request = new Request(
            $_GET,
            $_POST,
            [],
            $_COOKIE,
            $_FILES,
            $_SERVER
        );

        $method = $request->getMethod();

        return $this->render('main/main.html.twig', ['method' => $method]);
    }

    /**
     * @Route("/e48e13207341b6bffb7fb1622282247b")
     * @return Response
     */
    public function admin(KernelInterface $kernel)
    {

        $url = 'http://10.0.142.5'; // IP de l'API
        $call_api = new CallAPI();
        $request = new Request(
           $_GET,
           $_POST,
           [],
           $_COOKIE,
           $_FILES,
           $_SERVER
       );
       $method = $request->getMethod();

        $get_data = $call_api->callAPI('GET', $url.'?method='.$request->getMethod(), false);
        $response = $get_data;

        return $this->render('main/admin.html.twig', ['response' => json_decode($response), 'method' => $method]);
    }

    /**
     * @Route("/e48e13207341b6bffb7fb1622282247b/debug")
     * @return Response
     */
    public function debug(Profiler $profiler)
    {
        $profiler->enable();

        return $this->render('main/debug.html.twig');
    }

}
