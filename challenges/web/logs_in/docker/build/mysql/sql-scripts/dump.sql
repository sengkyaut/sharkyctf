USE db;

CREATE TABLE it_seems_secret (
id INT PRIMARY KEY NOT NULL,
flag  VARCHAR(200) NOT NULL
);

CREATE TABLE logs (
id INT PRIMARY KEY NOT NULL,
log_method VARCHAR(20) NOT NULL,
log_date   VARCHAR(20) NOT NULL
);

INSERT INTO it_seems_secret(id, flag) VALUES (1, 'shkCTF{CVE-2019-10913_S33m3D_Bulls5H1T_B3F0R3_TH15_Ch4LL_69e28c7b0004fe05b05800596e64343b}');

INSERT INTO logs(id, log_method, log_date) VALUES (1, 'GET', 'Wednesday 27th of November 2019 11:11:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (2, 'GET', 'Wednesday 27th of November 2019 11:12:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (3, 'GET', 'Wednesday 27th of November 2019 11:13:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (4, 'GET', 'Wednesday 27th of November 2019 11:14:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (5, 'GET', 'Wednesday 27th of November 2019 11:15:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (6, 'GET', 'Wednesday 28th of November 2019 11:16:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (7, 'GET', 'Wednesday 28th of November 2019 11:17:35 AM');


INSERT INTO logs(id, log_method, log_date) VALUES (8, 'POST', 'Wednesday 29th of November 2019 11:11:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (9, 'POST', 'Wednesday 29th of November 2019 11:12:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (10, 'POST', 'Wednesday 29th of November 2019 11:13:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (11, 'POST', 'Wednesday 29th of November 2019 11:14:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (12, 'POST', 'Wednesday 30th of November 2019 11:16:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (13, 'POST', 'Wednesday 30th of November 2019 11:17:35 AM');

INSERT INTO logs(id, log_method, log_date) VALUES (14, 'PUT', 'Wednesday 27th of November 2019 11:10:35 AM');
INSERT INTO logs(id, log_method, log_date) VALUES (15, 'PUT', 'Wednesday 27th of November 2019 11:09:35 AM');


GRANT SELECT ON *.* TO 'mainUser'@'%' IDENTIFIED BY 'th15Pa55w0rdIsTh3B35t';
