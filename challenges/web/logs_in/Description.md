# Part 1

Data printed on one of our dev application has been altered, after questioning the team responsible of its development, it doesn't seem to be their doing. The H4XX0R that changed the front seem to be a meme fan and enjoyed setting the front.

We've already closed the RCE he used, their is a sensible database running behind it. If anyone could access it we'll be in trouble. Can you please audit this version of the application and tell us if you find anything compromising, you shouldn't be able to find the admin session.

The application is hosted at [logs_in]: logs_in.sharkyctf.xyz

# Part 2

Whaaaat? They did not closed the dev mode? Seriously? Whatever, you couldn't access our sensible database, I guess an open dev application couldn't give that much information or hurt us could it?

The application is hosted at [logs_in]: logs_in.sharkyctf.xyz
