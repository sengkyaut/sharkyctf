#!/bin/sh

: ${MONGO_HOST:=containment_forum-mongo}
: ${MONGO_PORT:=27017}

cmd="$@"

until nc -z $MONGO_HOST $MONGO_PORT
do
    echo "Waiting for Mongo ($MONGO_HOST:$MONGO_PORT) to start..."
    sleep 0.5
done
