https://docs.mongodb.com/manual/reference/method/ObjectId/

This challenge is a basic IDOR (Insecure Direct Object Reference), we want to get one of the non indexed document id. To do this, we need to understand how is made an ObjectId with mongo!

a 4-byte timestamp value, representing the ObjectId’s creation, measured in seconds since the Unix epoch
a 5-byte random value
a 3-byte incrementing counter, initialized to a random value

So, we need three things.

- One existing Id, cause we cant guess a 5 byte random value
- The exact timestamp when the object was added to the database
- To increment by one the 3 byte counter

Hopefully, there is some indexed value, giving us the 5 byte random value, and the counter value. And the exact date of the creation of the document is shared with us!
