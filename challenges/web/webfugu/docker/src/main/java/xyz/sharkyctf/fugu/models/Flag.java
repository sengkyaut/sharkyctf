package xyz.sharkyctf.fugu.models;

public class Flag {
    private String content;
    
    public Flag(String content) {
        super();
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "Flag [content=REDACTED_IT_WONT_BE_THAT_EASY]";
    }
    
}