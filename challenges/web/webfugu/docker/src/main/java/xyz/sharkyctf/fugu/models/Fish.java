package xyz.sharkyctf.fugu.models;

public class Fish {

	private String name;
	private int discoveryYear;
	private String discovererName;
	
	public Fish(String name, int discoveryYear, String discovererName) {
		super();
		this.name = name;
		this.discoveryYear = discoveryYear;
		this.discovererName = discovererName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDiscoveryYear() {
		return discoveryYear;
	}

	public void setDiscoveryYear(int discoveryYear) {
		this.discoveryYear = discoveryYear;
	}

	public String getDiscovererName() {
		return discovererName;
	}

	public void setDiscovererName(String discovererName) {
		this.discovererName = discovererName;
	}

}
