package xyz.sharkyctf.fugu.controllers;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import xyz.sharkyctf.fugu.forms.FishForm;
import xyz.sharkyctf.fugu.models.Fish;
import xyz.sharkyctf.fugu.models.Flag;

@Controller
public class MainController {

    private static List<Fish> fishes = new ArrayList<Fish>();
    private static Integer MAX_SIZE = 1000;
    private static Flag flag = new Flag("shkCTF{w31rd_fishes_6ee9f7aabf6689c6684ad9fd9ffbae5a}");

    static {
        fishes.add(new Fish("Fugu alboplumbeus", 1845, "Richardson"));
        fishes.add(new Fish("Fugu basilevskianus", 1855, "Basilewsky"));
        fishes.add(new Fish("Fugu obscurus", 1949, "Abe"));
        fishes.add(new Fish("Fugu touphisodium", 1994, "Rajkowsky"));
        fishes.add(new Fish("Fugu nofiksus", 1999, "Idaproetsen"));
        fishes.add(new Fish("Fugu remsiomegabarbus", 1996, "Waibemaestro"));
        fishes.add(new Fish("Fugu fratsopedibus", 1997, "Canard"));
    }

	public String renderFromString(String htmlContent) {
        SpringTemplateEngine engine = new SpringTemplateEngine();

        Context ctx = new Context();
        ctx.setVariable("flag", flag);
        ctx.setVariable("fishes", fishes);

        if (htmlContent.toLowerCase().contains("java") ||htmlContent.toLowerCase().contains("runtime")) {
        	return "Bad Request";
        } else {
        	String result = engine.process(htmlContent, ctx);
        	if (result.contains("\"error\"") || result.contains("Process")) {
        		return "Bad Request";
        	}
        	return result;
        }
	}

    @GetMapping(value = { "/", "/index" })
    public String index(Model model) {

    	FishForm fishForm = new FishForm();
        model.addAttribute("fishForm", fishForm);

        return "index";
    }

    @RequestMapping(value = "/process", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public String process(@RequestParam String page) {
    	byte[] decodedBytes = Base64.getDecoder().decode(page);
    	String decodedString = new String(decodedBytes);

        return renderFromString(decodedString);
    }

}

