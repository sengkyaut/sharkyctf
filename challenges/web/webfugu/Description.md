# WebFugu

A new site listing the different species of fugu fish has appeared on the net.
Used by many researchers, it is nevertheless vulnerable.
Find the vulnerability and exploit it to recover some of the website configuration.

