const mongoose = require('mongoose');
const bodyParser = require('body-parser');
var express = require('express');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var app = express();

app.use( session({
   secret : 'vcSKC5w977i249YGS2HiyQm58Uu44jgXgCS',
   name : 'sessionId',
  })
);

app.use(express.static('public'))
// connect to Mongo daemon
mongoose
  .connect(
    'mongodb://mongo-express:y3do8TkH4EpW9eCMgHv6tUueLcaTSLanryE@mongo:27017/express-mongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));


// DB schema
const UserSchema = new mongoose.Schema({
  moneyspent: {
    type: Number,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  roles: {
    type: Array,
    required: true
  },
  password: {
    type: String,
    required: true
  }
});

User = mongoose.model('user', UserSchema);
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.render("index", { user : req.session.user, isAdmin : req.session.isAdmin });
});

app.get("/sharks", (req, res) => {
  res.render("sharks", { user : req.session.user, isAdmin : req.session.isAdmin });
});

app.get("/customers", (req, res) => {
  res.render("customers", { user : req.session.user, isAdmin : req.session.isAdmin });
});

app.get("/admin", (req, res) => {
  if (! req.session.user){
    res.redirect('/');
  } else {
    if (req.session.isAdmin == true){
      res.render("admin", {user : req.session.user, isAdmin : req.session.isAdmin, comment:"shkCTF{where_IS_MY_N0SQLI?_94dbabf632759f09956c283784408162}"});
    } else {
      res.render("admin", {user : req.session.user, isAdmin : req.session.isAdmin, comment:"Did you expect to get the flag without having an admin role?"});
    }
  }
});

app.post("/login", (req, res) => {

  query = {"username": req.body.username, "password":req.body.password};
  User.findOne(query,(err, user) => {
   if (err) throw err;
   if(user){
     req.session.user = user.username;
     if(user.roles.includes("ADMIN")){
       req.session.isAdmin = true;
     }
   }
   res.redirect(301, '/');
  });
});

app.post("/logout", (req, res) => {

  req.session.user = undefined;
  req.session.isAdmin = undefined;
  res.redirect(301, '/');

});

app.post('/users', (req, res) => {
  if (req.body['search[value]'] == '' || req.body['search[value]'] == undefined){
    query = {};
  } else {
    query = { $where: `this.username.match(/^${req.body['search[value]']}/)`};
  }

  if (req.body['start'] == '' || req.body['start'] == undefined){
    start = 0;
  } else {
    start = req.body['start'];
  }
  if (req.body['length'] == '' || req.body['length'] == undefined){
    length = 10;
  } else {
    length = req.body['length'];
  }

  User.countDocuments( {}, function(err, recordsTotal){
    User.find(query, {moneyspent:1, username:1, roles:1, _id:0}, { skip: parseInt(start), limit: parseInt(length) },(err, users) => {
      res.setHeader('Content-Type', 'application/json');
      if (err) users=[{username: err.errmsg, roles: [err.name], moneyspent: 0}];
      res.end(JSON.stringify({"totalPages": 4, "recordsTotal": recordsTotal,"recordsFiltered": recordsTotal,"data":users}));
    });
  });
})


const port = 3000;
app.listen(port, () => console.log('Server running...'));
