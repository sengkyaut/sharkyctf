#!/usr/bin/env python3
#coding: utf-8

import magic
import zipfile
from subprocess import run
from flask import Flask, request, render_template, jsonify, redirect
from hashlib import md5
from inspect import getsource 
from os import remove
from time import time

FIRST_PART_OF_FLAG = "shkCTF{Schr0diNgeR_"
SECOND_PART_OF_FLAG = "_w4s_A_gRea7_"
THIRD_PART_OF_FLAG = "_p01yg1o7_4e78011325dfbe4a05fd533ea422cc94}"

EXEC_PERM = 64
UPLOAD_FOLDER = 'uploads/'
app = Flask(__name__)

app.config['MAX_CONTENT_LENGTH'] = 20 * 1024 * 1024

# --- ( Save file ) ----
def name():
	timestamp = str(time())
	timestamp = bytes(timestamp, "utf-8")
	return md5(timestamp).hexdigest()

def save_file(file_to_save):
	file_dest = UPLOAD_FOLDER + name()
	file_to_save.save(file_dest)
	return file_dest

# --- ( File type checking ) ----
def check_magic(file_to_test):
    return magic.from_file(file_to_test).split(" ")[0]

def check_JPG(file_to_test):
	filetype = check_magic(file_to_test)
	return filetype == 'JPEG'

def check_ELF(file_to_test):
	filetype = check_magic(file_to_test)
	return filetype == 'ELF'

def check_PDF(file_to_test):
	cmd = [ './' + file_to_test ]
	return run(["pdfinfo"] + cmd, timeout=1).returncode == 0

def check_JAR(file_to_test):
    if zipfile.is_zipfile(file_to_test):
        zip_content = zipfile.ZipFile(file_to_test, 'r')
        contain_manifest = False
        contain_class = False

        for f in zip_content.filelist:
            if f.filename == "META-INF/MANIFEST.MF":
                contain_manifest = True
            elif f.filename[-6:] == ".class":
                contain_class = True

        return contain_manifest and contain_class

    return False


def put_aside(filename):
	remove(filename)

# --- ( Web server news ) ----
@app.route('/')
def index():
	return render_template("index.html")

@app.route('/upload', methods=["GET"])
def upload(error=None, msg=None):
	source_code = getsource(get_file)
	return render_template("upload.html", source_code=source_code, msg=msg, error=error)

@app.route('/upload', methods=["POST"])
def get_file():
	# Wenn die Anfrage eine Datei enthält
	if 'file' in request.files:
		new_file = request.files["file"]
		filename = save_file(new_file)

		# Überprüfen Sie das Dateiformat
		is_ELF = check_ELF(filename)
		is_PDF = check_PDF(filename)
		is_JAR = check_JAR(filename)
		is_JPG = check_JPG(filename)

		if is_ELF and is_JAR and is_PDF:
			put_aside(filename)
			return upload(msg=SECOND_PART_OF_FLAG)
		elif is_ELF and is_JAR:
			put_aside(filename)
			return upload(msg=FIRST_PART_OF_FLAG)
		elif is_JPG and is_JAR:
			put_aside(filename)
			return upload(msg=THIRD_PART_OF_FLAG)
		else:
			# Datei löschen
			remove(filename)
			return upload(error="Bad file")
	else:
		return upload(msg="No file detected")

@app.route('/receive', methods=["POST"])
def receive():
	return ('', 418)

if __name__ == '__main__':
	app.run(debug=False, host='0.0.0.0', port=9010)

