#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#define FALSE         0
#define TRUE          1
#define SEEK_DATA     3
#define BLOCK_NUMBER  5000

#ifndef OUTPUT_FILE
  #define OUTPUT_FILE "my_very_big_file"
#endif
#define PERV          "😏"
#ifndef DEBUG
  #define DEBUG       FALSE
#endif
int main(int argc, char const *argv[]) {
  if (argc != 2) {
    printf("Usage : %s <flag to hide>\n", argv[0]);
    exit(1);
  }

  int fd = open(OUTPUT_FILE, O_WRONLY | O_TRUNC | O_CREAT, 0644);
  __uint128_t off = 0;
  unsigned char smiley[4] = PERV;

  srand(time(NULL));

  if (DEBUG) printf("Wrote : %s at %llu\n", PERV, off);

  write(fd, &smiley, 4);

  for (size_t i = 0; i < strlen(argv[1]); i++) {
    int random_int = rand() % 25;

    for (int z = 0; z < 10000; z++) {
      off = lseek(fd, 4096 * BLOCK_NUMBER * random_int, SEEK_CUR);
    }

    if (DEBUG) printf("Wrote : %c at %llu\n", argv[1][i], off);

    write(fd, &argv[1][i], sizeof(char));
  }

  close(fd);

  return 0;
}
