Forensics : Romance Dawn
------------------------

Il suffit d'un pngcheck pour se rendre compte qu'un chunk nommé EASY est détecté comme incorrecte.

On peut facilement l'éditer en faisant un simple :
> mv 7uffy.png 7uffy

Puis en ouvrant son éditeur de texte favori :
> subl 7uffy

Dans mon éditeurs si je recherche le code héxa du chunk illégal :
> 4541 5359

Je me rends alors compte qu'il y a 4 chunks avec ce nom.
Il ne me reste qu'à remplacer les "EASY" par "IDAT".

 Soit :
 > 5451 5359

Puis :
> mv 7uffy 7uffy.png
> eog 7uffy.png

Un flag s'affiche :-)

Flag : shkCTF{7uffy_1s_pr0ud_0f_y0u_0a2a9795f0bdf8d17e4}
