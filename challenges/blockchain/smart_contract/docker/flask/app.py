from flask import Flask, render_template, url_for, redirect, session
from instance import Instance
import jinja2_highlight
import pygments
import uuid

class MyFlask(Flask):
    jinja_options = dict(Flask.jinja_options)
    jinja_options.setdefault('extensions',[]).append('jinja2_highlight.HighlightExtension')

app = MyFlask(__name__)
app.secret_key = b"th1s_1s_my_cr4zy_s3cr3t_key"

contracts = ["warmup.sol", "logic.sol", "guessing.sol", "multipass.sol", "shashasha.sol"]
flags = ["shkCTF{th4t_w4s_4n_1ns4n3_w4rmup_65c8522c0f36ed2566afa7}", "shkCTF{sh4m3_0n_y0u_l1ttl3_byt3_f0f6145540ea8c6ee8067c}", "shkCTF{bl0ckch41n_c0uld_b3_h3lpfull_05b12d40c473800270981b}", "shkCTF{wr1t1ng_4_c0ntr4ct_t0_3xpl01t_4n_0th3r_2501dbec0821}", "shkCTF{wh4t_th3_fuck_w1th_th4t_sh43_6aac18bebf505b9b1291477}"]

users_instances = {}
transaction_count = 1000

@app.route("/", methods=["GET"])
def index():
	session["uuid"] = str(uuid.uuid4())
	session["logged_in"] = True
	users_instances[session["uuid"]] = {contract:Instance(contract[0].upper() + contract[1:-4], open("./contracts/" + contract, "r").read()) for contract in contracts}
	return render_template("index.html")

@app.route("/help", methods=["GET"])
def help():
	return render_template("help.html")

@app.route("/level/<int:level>", methods=["GET"])
def level(level=None):
	if session.get("logged_in") != True:
		return redirect(url_for("index"))
	global users_instances
	if level in range(0,5):
		code = open("./contracts/" + contracts[level], "r").read()
		return render_template("level.html", level=level, code=code, address=users_instances[session["uuid"]][contracts[level]].getAddress())	
	else:
		return render_template("404.html")	

@app.route("/deployContract/<int:level>", methods=["POST"])
def deployContract(level=None):
	global users_instances
	global transaction_count
	if level in range(0,5):
		transaction_count += 1
		code = open("./contracts/" + contracts[level], "r").read()
		users_instances[session["uuid"]][contracts[level]].deploy(level, transaction_count)
		return redirect(url_for("level", level=level))
	else:
		return render_template("404.html")

@app.route("/askFlag/<int:level>", methods=["POST"])
def askFlag(level=None):
	global users_instances
	global flags
	if level in range(0,5) and "You will need" not in users_instances[session["uuid"]][contracts[level]].getAddress():
		code = open("./contracts/" + contracts[level], "r").read()
		money = users_instances[session["uuid"]][contracts[level]].returnStorage()
		flag = flags[level] if money == 0 else "¯\\_(ツ)_/¯"
		return render_template("level.html", level=level, code=code, address=users_instances[session["uuid"]][contracts[level]].getAddress(), flag=flag)	
	elif level in range(0,5):
		code = open("./contracts/" + contracts[level], "r").read()
		return render_template("level.html", level=level, code=code, address=users_instances[session["uuid"]][contracts[level]].getAddress(), flag="Please instanciate the contract")	

	else:
		return render_template("404.html")

@app.errorhandler(404)
def page_not_found(error):
   return render_template('404.html')

if __name__ == "__main__":
	app.run()

