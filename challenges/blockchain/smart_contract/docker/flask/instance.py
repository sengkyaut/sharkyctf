from solc import compile_source
from web3 import Web3, HTTPProvider
import json

class Instance:
	def __init__(self, name, code):
		self.address = "You will need to wait a bit during the transaction."
		self.compiled = compile_source(code)
		self.contract_interface = self.compiled['<stdin>:' + name]
		self.w3 = Web3(HTTPProvider("https://ropsten.infura.io/v3/6e8cc4f13d12484c8b1e4cda0d2baae3")) 
		self.contract = self.w3.eth.contract(abi=self.contract_interface["abi"], bytecode=self.contract_interface["bin"])
		self.acct = self.w3.eth.account.privateKeyToAccount("0x5841C88A412FDF0DBF615E0592C5C622EBCE23661776AD1ABBF31311B43361C3")

	def getAddress(self):
		return self.address

	def deploy(self, level, count):
		if level != 2:
			construct_txn = self.contract.constructor().buildTransaction({
			    'from': self.acct.address,
			    'value': self.w3.toWei('0.005', 'ether'),
			    'nonce': self.w3.eth.getTransactionCount(self.acct.address, 'pending'),
			    'gas': 1000000,
			    'gasPrice': self.w3.toWei('30', 'gwei')})
		else:
			passphrase = "0x49276d2070723374747920737572332079307520627275743366307263336421"	
			construct_txn = self.contract.constructor(passphrase).buildTransaction({
			    'from': self.acct.address,
			    'value': self.w3.toWei('0.005', 'ether'),
			    'nonce': self.w3.eth.getTransactionCount(self.acct.address, 'pending'),
			    'gas': 1000000,
			    'gasPrice': self.w3.toWei('30', 'gwei')})
		signed = self.acct.signTransaction(construct_txn)
		txn_hash = self.w3.eth.sendRawTransaction(signed.rawTransaction)
		txn_receipt = self.w3.eth.waitForTransactionReceipt(txn_hash)
		self.address = txn_receipt["contractAddress"]

	def returnStorage(self):
		return self.w3.eth.getBalance(self.address)