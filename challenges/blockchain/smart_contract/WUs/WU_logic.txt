On copie colle le contrat sur Remix, on le déploie à l'adresse donnée lors de l'instanciation.
Ensuite le challenge se solve en deux temps :
- Premièrement on appelle la fonction claim() en donnant la bonne passphrase + 0.05 ether de value
- Ensuite on appelle la fonction withdraw()

Attention cependant, la passphrase semble être en ASCII mais c'est un type bytes32.
Si on ne sait pas ce que c'est, on peut récupérer la valeur de la variable passphrase de cette manière :
> web3.eth.getStorageAt("<addresse du contrat déployé>", 1, web3.eth.defaultBlock, console.log)

Qui nous donne :
0x74683320666c3467203173206e30742068337233000000000000000000000000

Ensuite il ne reste qu'à réclamer le flag sur l'interface des smart contracts :
shkCTF{sh4m3_0n_y0u_l1ttl3_byt3_f0f6145540ea8c6ee8067c}
