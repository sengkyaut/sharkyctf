#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "aes.h"
// #define OUTPUT_FILE_NAME "./privateLeaked.sqlite"
#define TRUE             1
#define FALSE            0
#ifndef DEBUG
  #define DEBUG          FALSE
#endif
#define BUFFER_SIZE      4096
// #ifndef SERVER_ADDR
  // #define SERVER_ADDR    "0.0.0.0"
// #endif
#ifndef HOST_PORT
  #define HOST_PORT    7001
#endif
#ifndef HOSTNAME
  #define HOSTNAME "149.202.221.103"
#endif

#define DOGE_ASCII "░░░░░░░░░▄░░░░░░░░░░░░░░▄░░░░\n\
░░░░░░░░▌▒█░░░░░░░░░░░▄▀▒▌░░░\n\
░░░░░░░░▌▒▒█░░░░░░░░▄▀▒▒▒▐░░░\n\
░░░░░░░▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐░░░\n\
░░░░░▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐░░░\n\
░░░▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌░░░\n\
░░▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒▌░░\n\
░░▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐░░\n\
░▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄▌░\n\
░▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒▌░\n\
▀▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒▐░\n\
▐▒▒▐▀▐▀▒░▄▄▒▄▒▒▒▒▒▒░▒░▒░▒▒▒▒▌\n\
▐▒▒▒▀▀▄▄▒▒▒▄▒▒▒▒▒▒▒▒░▒░▒░▒▒▐░\n\
░▌▒▒▒▒▒▒▀▀▀▒▒▒▒▒▒░▒░▒░▒░▒▒▒▌░\n\
░▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▒▄▒▒▐░░\n\
░░▀▄▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▄▒▒▒▒▌░░\n\
░░░░▀▄▒▒▒▒▒▒▒▒▒▒▄▄▄▀▒▒▒▒▄▀░░░\n\
░░░░░░▀▄▄▄▄▄▄▀▀▀▒▒▒▒▒▄▄▀░░░░░\n\
░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▀▀░░░░░░░░\n"

// #define PASSWORD_TO_FIND "T4h7s_4ll_F0lks"  // this is not used in compilation

extern unsigned char *_start;
extern unsigned char *__etext;
char userInputBuffer[80] = { 0 };
char *cantReverse = "You can't debug me :*";
char *encPassword = "N3kviX7-vXEqvlp";
unsigned long sum = 0;
unsigned char valid = TRUE;
unsigned char breakpointCount = 0;

// Takes integer, parse each byte and xor user input with those
// Also xor it with the nb of breakpoints found at startup to prevent dynamic debug
void cypher(int cofebabe) {
  #ifdef ANTI_FUNCTION_ENTRY
  //anti disas
  __asm__ __volatile__ (
    "call .+5\n\t"
    "add dword ptr [esp], 5\n\t"
    "ret\n\t"
    );
  #endif

  unsigned int size = strlen(userInputBuffer);

  for (size_t count = 0; count < 1; count++) {
    for (size_t i = 0; i < size; i++) {
      userInputBuffer[i] ^= ((cofebabe >> (8 * (i % 4) )) & 0xff) ^ breakpointCount;
    }
  }

  if (DEBUG) {
    printf("U PASSWORD after xor %s\n", userInputBuffer);
  }

  return;
}

void checkPasswordLen(char *password) {
  valid = (strlen(password) == 15);
  return;
}

#ifdef XORED_WITH_BP
long int getIntegrity() {
  // anti disas
  #ifdef ANTI_FUNCTION_ENTRY
  __asm__ __volatile__ (
    "call .+5\n\t"
    "add dword ptr [esp], 5\n\t"
    "ret\n\t"
    );
  #endif
  char *start = (char *)&_start;
  char *end = (char *)&__etext;
  unsigned long int count = 0;
  volatile unsigned char halfBP[1] = { 0x66 };

  if (DEBUG) {
    printf("_start @ %p\n", start);
    printf("__etext @ %p\n", end);
    printf("Offset end - start = %ld \n", end - start);
  }

  if (DEBUG) {
    printf("Breakpoint value = %x\n", (*halfBP) + (*halfBP));
  }


  while (start != (end)) {

    if (((*(volatile unsigned *)start) & 0xFF) == (*halfBP) + (*halfBP)) {
      if (DEBUG) {
        printf("Breakpoint found\n");
      }
      puts("Stop debug me");

      breakpointCount += 1;
    }

    count += ((*(volatile unsigned *)start) & 0xFF) % ULONG_MAX;
    ++start;
  }

  if (DEBUG) {
    printf("checksum = %ld\n", count);
  }

  return count;
}

#endif

// Returns the size of the file
long int recvDatabase(const char *key, const char* argv1) {
  uint8_t iv[] = { 0x71, 0x01, 0x02, 0x03, 0x04, 0x48, 0x06, 0x07, 0x88, 0x09, 0x0a, 0xfe, 0x0c, 0x4d, 0x0e, 0x0f };

  int socket_desc;
  char buffer[16] = { 0 };
  char output[16] = { 0 };


  FILE *output_file;

  if (NULL == (output_file = fopen(argv1, "w+"))) {
    perror("fopen");
    exit(1);
  }

  socket_desc = socket(AF_INET, SOCK_STREAM, 0);

  if (socket_desc == -1) {
    perror("Could not create socket");
    return -1;
  }

  struct sockaddr_in server;
  server.sin_addr.s_addr = inet_addr(HOSTNAME);
  server.sin_family = AF_INET;
  server.sin_port = htons(HOST_PORT);


  puts("Contacting server");

  // if (connect(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0) {
  if (connect(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0) {
    puts("connect error");
    return -1;
  }

  puts("Retrieving to DB, decrypting it using your password.");
  int y = 0;
  size_t fileSize = 0;

  do {
    memset(buffer, 0x0, sizeof buffer);
    memset(output, 0x0, sizeof output);
    int chunkSize = 0;
    recv(socket_desc, &chunkSize, 4, MSG_WAITALL);

    if (DEBUG) {
      printf("SIZE OF NEXT CHUNK %d\n", chunkSize);
    }

    y = recv(socket_desc, buffer, chunkSize, MSG_WAITALL);
    fileSize += chunkSize;


    AES_CBC_decrypt_buffer(output, buffer, 16, key, iv);

    if (DEBUG) {
      printf("OUTPUT | %s\n", output);
      printf("LEN of recved %d\n", y);
    }

    fwrite(output, chunkSize, 1, output_file);

    if (y < 0) {
      puts("Connection error");
    }
  } while (y == 16);
  printf("File downloaded. %d bytes.\n", fileSize);
  fclose(output_file);
  puts("Done. Check your output file.");
  close(socket_desc);
}


int cmp_strs(char* in, char* sec, int size){
  for (size_t i = 0; i < size; i++) {
    printf("cmp %c to %c\n", in[i], sec[i]);
    if(in[i] != sec[i]) return FALSE;
  }
  puts("returned true");
  return TRUE;
}

int main(int argc, char const *argv[]) {
  if(argc != 2){
    printf("Usage : %s output_file\n", argv[0]);
    exit(1);
  }
  #ifdef XORED_WITH_BP
  sum = getIntegrity();
  #else
  sum = 0;
  #endif


  char toCompute[16] = { 0 };
  printf("Hi Doge ! \n"DOGE_ASCII"Please input the password :\n-> ");
  fgets(userInputBuffer, sizeof userInputBuffer, stdin);
  userInputBuffer[strcspn(userInputBuffer, "\n")] = '\0';
  strncpy(toCompute, userInputBuffer, sizeof toCompute);

  // Some anti-disas stuff
  __asm__ __volatile__ (
    // "jmp someRet\n\t"
    "xor edx, edx\n\t"
    "mov dx, 0x05eb\n\t"
    "xor eax, eax\n\t"
    "jz .-4\n\t"
    ".byte 0xe8\n\t"
    "mov eax, 0x20817da1\n\t" // pushing half 0x4103071a
    "jz .+5\n\t"
    "jnz .+3\n\t"
    ".byte 0xe8\n\t"
    "inc eax\n\t"
    "add eax, edx\n\t"
    "push eax\n\t"
    // "push 0x080493c0\n\t"
    "xor eax, eax\n\t"
    "jz .+17\n\t"
    "add dword ptr [esp], 0x2081838d\n\t" // 7 bytes
    "call cypher\n\t"  // 6
    "jz .+5\n\t" // 2
    ".byte 0xe8\n\t"
    "jz .-15\n\t" // 2
    // "nop\n\t"
    );
  if(DEBUG){printf("User passwd encypted %s\n", userInputBuffer);}
  // printf("%d\n", cmp_strs(userInputBuffer, encPassword, 15));
  if (cmp_strs(userInputBuffer, encPassword, 15) == FALSE) {
    printf("Wrong password sorry, exiting.\n");
    return EXIT_FAILURE;
  } else {
    puts("The password is valid.");
  }

  recvDatabase(toCompute, argv[1]);

  return 0;
}
