
from z3 import *

def ascii_printable(x):
    return And(0x20 <= x, x <= 0x7F)

KEY_LEN = 24

out = [BitVec("{}".format(i), 8) for i in range(KEY_LEN)]
s = Solver()

# s.add(And(map(ascii_printable, out)))

s.add( (out[20] ^ 43) == out[7] )
s.add( (out[21] - out[3])  == -20 )
s.add( (out[2] >> 222 % 8) == 0 )
s.add( (out[13] + 87) == 203 )
s.add( (out[11] << 82 % 8) == 380 )
s.add( (out[7] >> out[17] % 8) == 5 )
s.add( (out[6] ^ 83) == out[14] )
s.add( (out[8] - 63) == 59 )
s.add( (out[5] << out[9] % 8) == 392 )
s.add( (out[16] - out[7])  == 20 )
s.add( (out[7] << out[23] % 8) == 190 )
s.add( (out[2] - out[7])  == -43 )
s.add( (out[21] - 131) == -36 )
s.add( (out[2] ^ 71) == out[3] )
s.add( (out[0] + 208) == 307 )
s.add( (out[13] << 64 % 8) == 116 )
s.add( (out[20] & 69) == 68 )
s.add( (out[8] & 21) == 16 )
s.add( (out[12] - 116) == -21 )
s.add( (out[4] >> 204 % 8) == 7 )
s.add( (out[13] ^ 71) == 51 )
s.add( (out[0] >> out[0] % 8) == 12 )
s.add( (out[10] ^ 243) == 172 )
s.add( (out[8] & 172) == 40 )
s.add( (out[16] + 40) == 155 )
s.add( (out[22] & 29) == 24 )
s.add( (out[9] + 39) == 90 )
s.add( (out[5] - 71) == -22 )
s.add( (out[19] << 194 % 8) == 456 )
s.add( (out[20] >> 46 % 8) == 1 )
s.add( (out[7] >> 121 % 8) == 47 )
s.add( (out[1] + 232) == 340 )
s.add( (out[3] >> 244 % 8) == 7 )
s.add( (out[19] & 73) == 64 )
s.add( (out[4] ^ 124) == 15 )
s.add( (out[2] & out[11]) == 20 )
s.add( (out[0] & out[0]) == 99 )
s.add( (out[4] + out[5])  == 164 )
s.add( (out[15] << 30 % 8) == 6080 )
s.add( (out[10] ^ 43) == out[17] )
s.add( (out[12] ^ 44) == out[4] )
s.add( (out[19] - out[21])  == 19 )
s.add( (out[12] - 210) == -115 )
s.add( (out[12] - 71) == 24 )
s.add( (out[15] >> 193 % 8) == 47 )
s.add( (out[19] - 103) == 11 )
s.add( (out[17] + out[18])  == 168 )
s.add( (out[22] ^ 78) == 116 )
s.add( (out[23] & out[21]) == 9 )
s.add( (out[6] << out[19] % 8) == 396 )
s.add( (out[3] + out[7])  == 210 )
s.add( (out[22] & 237) == 40 )
s.add( (out[12] & 172) == 12 )
s.add( (out[18] ^ 107) == out[15] )
s.add( (out[16] & 122) == 114 )
s.add( (out[0] & 57) == 33 )
s.add( (out[6] ^ 60) == out[21] )
s.add( (out[20] >> 96 % 8) == 116 )
s.add( (out[19] + 194) == 308 )
s.add( (out[12] << 16 % 8) == 95 )
s.add( (out[2] ^ 206) == 250 )
s.add( (out[23] ^ 238) == 199 )
s.add( (out[10] << 40 % 8) == 95 )
s.add( (out[22] & out[9]) == 50 )
s.add( (out[3] + out[2])  == 167 )
s.add( (out[17] - out[14])  == 68 )
s.add( (out[21] + 112) == 207 )
s.add( (out[19] ^ 45) == out[10] )
s.add( (out[12] << 2 % 8) == 380 )
s.add( (out[6] & 64) == 64 )
s.add( (out[12] & out[22]) == 26 )
s.add( (out[7] << out[19] % 8) == 380 )
s.add( (out[4] ^ 0) == out[4] )
s.add( (out[20] ^ 78) == out[22] )
s.add( (out[6] ^ 229) == 134 )
s.add( (out[12] - out[7])  == 0 )
s.add( (out[19] - out[13])  == -2 )
s.add( (out[14] >> 212 % 8) == 3 )
s.add( (out[12] & 56) == 24 )
s.add( (out[8] << out[10] % 8) == 15616 )
s.add( (out[20] ^ 98) == 22 )
s.add( (out[6] >> out[22] % 8) == 24 )
s.add( (out[22] - out[5])  == 9 )
s.add( (out[7] << out[22] % 8) == 380 )
s.add( (out[22] - 153) == -95 )
s.add( (out[16] + 3) == 118 )
s.add( (out[23] ^ 29) == out[18] )
s.add( (out[23] + out[14])  == 89 )
s.add( (out[5] & out[2]) == 48 )
s.add( (out[15] & 159) == 31 )
s.add( (out[4] - 2) == 113 )
s.add( (out[23] ^ 74) == out[0] )
s.add( (out[6] ^ 60) == out[11])



while s.check() == sat:
    model = s.model()
    print("Found")
    res = ""
    for i in range(KEY_LEN):
        res += chr(model[out[i]].as_long())
    print(res)
    s.add(Or(out[0] != s.model()[out[0]],
out[1] != s.model()[out[1]],
out[2] != s.model()[out[2]],
out[3] != s.model()[out[3]],
out[4] != s.model()[out[4]],
out[5] != s.model()[out[5]],
out[6] != s.model()[out[6]],
out[7] != s.model()[out[7]],
out[8] != s.model()[out[8]],
out[9] != s.model()[out[9]],
out[10] != s.model()[out[10]],
out[11] != s.model()[out[11]],
out[12] != s.model()[out[12]],
out[13] != s.model()[out[13]],
out[14] != s.model()[out[14]],
out[15] != s.model()[out[15]],
out[16] != s.model()[out[16]],
out[17] != s.model()[out[17]],
out[18] != s.model()[out[18]],
out[19] != s.model()[out[19]],
out[20] != s.model()[out[20]],
out[21] != s.model()[out[21]],
out[22] != s.model()[out[22]],
out[23] != s.model()[out[23]],
))
