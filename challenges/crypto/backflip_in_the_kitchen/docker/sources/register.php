<?php
include 'configuration.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL | E_STRICT);

// Try and connect using the info above.
$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
if ( mysqli_connect_errno() ) {
	// If there is an error with the connection, stop the script and display the error.
	die ('Failed to connect to MySQL: ' . mysqli_connect_error());
}

// Now we check if the data from the login form was submitted, isset() will check if the data exists.
if ( !isset($_POST['username'], $_POST['password']) ) {
	// Could not get the data that should have been sent.
	die ('Please fill both the username and password field!');
}elseif(strlen($_POST['username']) > 255 || strlen($_POST['password']) > 255){
    die('Please use no more than 255 characters for username and password');
}else{
    if ($stmt = $con->prepare('SELECT * FROM users WHERE username = ?')) {
        $stmt->bind_param('s', $_POST['username']);  
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows === 0) {
            $stmt->close();
            if ($stmt = $con->prepare('INSERT INTO users (username, password) VALUES(?, ?)')) {
                $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
                $stmt->bind_param('ss', $_POST['username'], $password);
                $success = $stmt->execute();
                if($success === true){
                    echo "<p>Welcome " . $_POST['username'] . " ! You can now get <a href='/index.html'>authenticated</a> !</p>";
                }
                else{
                    echo "<p>Something went wrong, try another username or contact the administrator.</p>";
                }
                $stmt->close();
            }else{
                echo "<h1>The coconut nut is a giant nut, if you eat too much you'll get very fat.</h1>";
                var_dump($con->error_list);
            }
        }else{
            die("Oh come on! This user already exist! Now hurry and get <a href='/index.html'>authenticated</a> please!");
        }
    }
}

