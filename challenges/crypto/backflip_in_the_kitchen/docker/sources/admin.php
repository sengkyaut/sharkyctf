<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Admin panel</title>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
		<link href="style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
<?php
require 'configuration.php';
$algo="AES-128-CBC";

function decrypt($ciphertext, $cipher, $key)
/**
 * Decrypt a base64 ciphertext
 *
 * Return plaintext
 */
{
    $c = base64_decode($ciphertext);
    $ivlen = openssl_cipher_iv_length($cipher);
    $iv = substr($c, 0, $ivlen);
    $ciphertext_raw = substr($c, $ivlen);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    return $original_plaintext;
}

if(!isset($_COOKIE["authentication_token"])) {
    die("You are not authenticated");
}else{
    $token = json_decode(decrypt($_COOKIE["authentication_token"], $algo, $SECRET_KEY));
    if($token === NULL) {
        die("BAD TOKEN, CAN'T PARSE YOU! YOU MEAN TOKEN! BAD TOKEN!");
    }
    if($token->is_admin === 1){
        echo "<p>Well done boy, here is the flag : $FLAG</p>";
    }else{
        echo "<h1>Hello $token->username. I'm sorry but you are not allowed to see what's in there. peace.</h1>";
        echo "<p>Ah shit, here we go again!</p>";
    }
}
?>

		<p><a href="/logout.php">Get lost...</a></p>
	</body>
</html>
