<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Profile page</title>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
		<link href="style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
<?php
require 'configuration.php';
$algo="AES-128-CBC";

function decrypt($ciphertext, $cipher, $key)
/**
 * Decrypt a base64 ciphertext
 *
 * Return plaintext
 */
{
    $c = base64_decode($ciphertext);
    $ivlen = openssl_cipher_iv_length($cipher);
    $iv = substr($c, 0, $ivlen);
    $ciphertext_raw = substr($c, $ivlen);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    return $original_plaintext;
}

if(!isset($_COOKIE["authentication_token"])) {
    die("You are not authenticated");
}else{
    $decrypted = decrypt($_COOKIE["authentication_token"], $algo, $SECRET_KEY);
    $token = json_decode($decrypted);
		if (isset($_COOKIE["debug"]) && $_COOKIE["debug"] === "true") {
				echo "<script>console.log($decrypted);</script>";
		}
    if($token === NULL) {
        die("BAD TOKEN, CAN'T PARSE YOU! YOU MEAN TOKEN! BAD TOKEN!");
    }else{
        echo "<h1>Welcome to your profile page, $token->username </h1>";
        echo "<p>Here are the information we store about you :</p>";
        echo "<ul>";
        echo "    <li>ID : $token->id </li>";
        echo "    <li>Username : $token->username </li>";
        echo "    <li>Administrator : $token->is_admin </li>";
        echo "</ul>";
    }
}
?>

			<p><a href="/admin.php">Admin panel</a></p>
	</body>
</html>
