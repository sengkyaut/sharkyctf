#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

//gcc -O0 -Wl,-z,relro,-z,now -fno-stack-protector-all -o ../ressources/captain_hook captain_hook.c

typedef struct character{
  char name[32];
  int age;
  char date[32];
}Character;


Character *jail[4] = {0};


void init_buffering(){
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);
}

int read_user_int(){
	char buf[9];
	int i;

	fgets(buf, 8, stdin);
	i = atoi(buf);

	return i;
}

void read_user_str(char* s, int size){
	char *ptr = NULL;
	fgets(s, size, stdin);
	ptr = strchr(s, '\n');
	if(ptr != NULL)
		*ptr = 0;
}

int check_date_format(char *date){
  for(int i = 0; i < 10 ; i++){
    if(i != 2 && i != 5){
      if(date[i] < 0x30 || date[i] > 0x39)
        return 0;

    }
    else{
      if(date[i] != 0x2f)
        return 0;

    }
  }

  return 1;
}

void lock_up_character(){

	int i;
	Character *character = NULL;

  printf(" [ Character index ]: ");
	i = read_user_int();
	if(i < 0 || i >= 4 || jail[i] != NULL){
		printf("  [!] Invalid index.\n");
		return;
	}

	character = malloc(sizeof(Character));

	if(character == NULL)
		exit(-1);

	puts(" [ Character ]");
	printf("  Name: ");
	read_user_str(character->name, 31);
	printf("  Age: ");
	character->age = read_user_int();
  printf("  Date (mm/dd/yyyy): ");
	read_user_str(character->date, 11);

	jail[i] = character;
}

void free_character(){
	Character *character = NULL;
	int i;

	printf(" [ Character index ]: ");
	i = read_user_int();

	if(i < 0 || i >= 4 || jail[i] == NULL){
		printf("  [!] Invalid index.\n");
		return;
	}

	character = jail[i];

	free(character);
	jail[i] = NULL;
}

void read_character_infos(){

  Character *character = NULL;
  char buf[32];
  int n;
	int i;

	printf(" [ Character index ]: ");
	i = read_user_int();

	if(i < 0 || i >= 4 || jail[i] == NULL){
		printf("  [!] Invalid index.\n");
		return;
	}

	character = jail[i];

  strncpy(buf, character->name, 32);
  printf("Character name: %s\n", buf);
  n = character->age;
  printf("Age: %d\n", n);
  strncpy(buf, character->date, 32);
  printf("He's been locked up on ");
  if(check_date_format(character->date)){
    printf(character->date);
  }
  else{
    printf("an invalid date.");
  }
  puts(".");
}

void edit_character(){
  int i;
  Character *character = NULL;
	char buf[32];
  int n;

  printf(" [ Character index ]: ");
	i = read_user_int();
	if(i < 0 || i >= 4 || jail[i] == NULL){
		printf("  [!] Invalid index.\n");
		return;
	}

	character = jail[i];

	puts(" [ Character ]");
	printf("  Name: ");
	read_user_str(buf, 127);
  if(strcmp(character->name, buf))
    strncpy(character->name, buf, 32);

	printf("  Age: ");
	n = read_user_int();
  if(character->age != n)
    character->age = n;

  printf("  Date (mm/dd/yyyy): ");
	read(0, buf, 10);
  if(strcmp(character->date, buf))
    strncpy(character->date, buf, 32);

}

void list_characters(){
	int i;
	Character *character = NULL;
	for(i = 0 ; i < 4 ; i++){
		if(jail[i] != NULL){
			character = jail[i];
			printf(" [%2d]: %s, %d\n", i, character->name, character->age);
		}
    else{
      printf(" [%2d]: ------\n", i);
    }
	}
}

void quit(){
	printf("Have a nice day !\n");
	exit(0);
}

void print_commands(){
	printf("\n==Commands========\n" \
				 " 1 -> List all characters\n" \
				 " 2 -> Lock up a new character\n" \
				 " 3 -> Read character infos\n" \
         " 4 -> Edit character infos\n" \
				 " 5 -> Free a character\n" \
				 " 6 -> Quit\n" \
				 "==================\n\n"
	);
}

int main(void){

  int choice;
  int i;

	init_buffering();
	print_commands();

	for(i = 0 ; i < 40 ; i++){
		printf("peterpan@pwnuser:~$ ");
		choice = read_user_int();

		switch (choice) {
			case 0:
				print_commands();
				break;
			case 1:
				list_characters();
				break;
			case 2:
				lock_up_character();
				break;
			case 3:
				read_character_infos();
				break;
      case 4:
				edit_character();
				break;
			case 5:
				free_character();
				break;
			case 6:
				quit();
				break;
			default:
				printf("\n[!] Invalid command. Enter '0' for available commands.\n\n");
				break;
		}
	}

  return 0;
}
