# Free Captain Hook

## Difficulty

Medium

## Description

Find a way to pop a shell.

## Documentation

Format string, 64bits.

## Setup

```bash
cd docker
docker build . -t captain_hook
docker run -d --name captain_hook -p 20336:20336 -m 32m --memory-swap 32m --read-only --restart always --cpus=".1" captain_hook
```

## Ressources

Le contenu du dossier "ressources" est à fournir au challenger.

## Solution

Exploiter la format string pour leak la libc, puis écrire l'adresse de system dans __free_hook, et finir par free un chunk préalablement malloced pour appeler system("/bin/sh").

```bash
python2.7 exploit.py
```
