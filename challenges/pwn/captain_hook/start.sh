#!/bin/bash

cd docker
docker build . -t captain_hook
docker run -d --name captain_hook -p 20336:20336 -m 32m --memory-swap 32m --read-only --restart unless-stopped --cpus=".1" captain_hook

