#!/bin/bash

cd docker
docker build . -t give_away_2
docker run -d --name give_away_2 -p 20335:20335 -m 32m --memory-swap 32m --read-only --restart unless-stopped --cpus=".1" give_away_2

