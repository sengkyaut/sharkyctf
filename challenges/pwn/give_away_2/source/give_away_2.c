#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//gcc -O0 -Wl,-z,relro,-z,now -fno-stack-protector -o ../ressources/give_away_2 give_away_2.c

void init_buffering(){
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);
}

void vuln(){
  char buf[24];
  fgets(buf, 128, stdin);
}

int main(void){

  init_buffering();

  printf("Give away: %p\n", main);
  vuln();

  return 0;
}
