# Give Away #2

## Difficulty

Easy #wamup

## Description

Make good use of this gracious give away.

## Documentation

Basique stack buffer overflow 64 bits.

## Setup

```bash
cd docker
docker build . -t give_away_2
docker run -d --name give_away_2 -p 20335:20335 -m 32m --memory-swap 32m --read-only --restart always --cpus=".1" give_away_2
```

## Ressources

Le contenu du dossier "ressources" est à fournir au challenger.

## Solution

À partir de l'adresse de main, calculer les adresses sur lesquelles jumps pour construire sa ROP chain, qui aura pour but de leak la position de la libc, de réappeler la fonction vulnerable, puis réexploiter le buffer overflow pour faire pop un shell.

```bash
python2.7 exploit.py
```
