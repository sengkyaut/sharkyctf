# Give Away #1

## Difficulty

Easy #warmup

## Description

Make good use of this gracious give away.

## Documentation

Basique stack buffer overflow 32 bits.

## Setup

```bash
cd docker
docker build . -t give_away_1
docker run -d --name give_away_1 -p 20334:20334 -m 32m --memory-swap 32m --read-only --restart always --cpus=".1" give_away_1
```

## Ressources

Le contenu du dossier "ressources" est à fournir au challenger.

## Solution

Calculer l'adresse de "/bin/sh" dans la libc à partir de l'adresse de system (give away), puis écraser la save d'eip pour appeler system("/bin/sh").

```bash
python2.7 exploit.py
```
