#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//gcc -m32 -O0 -Wl,-z,relro,-z,now -fno-stack-protector -o give_away_1 give_away_1.c

void init_buffering(){
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);
}

void vuln(){
  char buf[24];
  fgets(buf, 50, stdin);
}

int main(void){

  init_buffering();

  printf("Give away: %p\n", system);
  vuln();

  return 0;
}
