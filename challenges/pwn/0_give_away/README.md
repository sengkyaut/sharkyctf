# 0 Give Away

## Difficulty

Easy #warmup

## Description

Free flag

## Documentation

Easy stack buffer overflow 64 bits.

## Setup

```bash
cd docker
docker build . -t 0_give_away
docker run -d --name 0_give_away -p 20333:20333 -m 32m --memory-swap 32m --read-only --restart always --cpus=".1" 0_give_away
```

## Ressources

Le contenu du dossier "ressources" est à fournir au challenger.

## Solution

```bash
python2.7 exploit.py
```
