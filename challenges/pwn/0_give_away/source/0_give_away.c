#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//gcc -fno-stack-protector -no-pie -o 0_give_away 0_give_away.c

void init_buffering(){
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);
}

void win_func(){
  execve("/bin/sh", NULL, NULL);
}

void vuln(){
  char buf[24];
  fgets(buf, 50, stdin);
}

int main(void){

  init_buffering();

  vuln();

  return 0;
}
